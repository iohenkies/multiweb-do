# Multi webservices with Docker compose

With this repo we'll be setting up a webserver serving multiple websites from containers.

We'll be working with some cool technologies like
* Cloud init
* Terraform
* Docker
* Docker compose
* Traefik
