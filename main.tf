# We are creating a separate project for our web01 droplet
resource "digitalocean_project" "multisite" {
  name        = "multisite"
  description = "The is our multisite project with the lone droplet we're creating below"
  purpose     = "Web Application"
  environment = "Development"
  resources   = [digitalocean_droplet.web01.urn]
}

# Create a new Web droplet for our multisite project in the Amsterdam region
resource "digitalocean_droplet" "web01" {
  image     = "ubuntu-20-04-x64"
  name      = "web01"
  region    = var.region
  size      = var.size
  tags      = var.tags
  backups   = var.backup
  ssh_keys  = [digitalocean_ssh_key.henk_batelaan.fingerprint]
  user_data = <<EOF
#cloud-config
apt:
  sources:
    docker.list:
      source: deb [arch=amd64] https://download.docker.com/linux/ubuntu $RELEASE stable
      keyid: 9DC858229FC7DD38854AE2D88D81803C0EBFCD88
groups:
  - docker
users:
  - name: cloud_deploy
    ssh-authorized-keys:
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAEAQCiH5L3q0KozYBdjM9H9oTLrZQx5os3VUN847eh1zrFkF4W35GRhT9h5rKJophVEvqtKDh1RTeubzQjzVmnMp4mo4XzAlApjFLkQtpcEGer+QhiQMmlMY+8yyl/+y+jUYWT/BGIHgkeHqJ8XLLqrQ+E8ROxSHciM5FEZxmfebcoRi7M19YJZKvbGXO8yIgX/YFk279VJkSLlNAPvmFACcb9Po60E4ca9eyd/g1jbLYUZGd7jARuxeICuIvZLQCFXoQH332i5xRSK7vgt6cqQP4JhNwJ40r0nge0zvzZfb1k9g+YaRwesHh4cU/iA9LqkjOynEdn6jsBphSMBiz3zo+Gx3qTxjLazBOjOwcL2TMNhnD8tU+iGk8zp7LlcJt7EgHvqEI6W09poW0ex/pHu2eQ/LjW0pGuRisGQu4Gb5HuzDHxZpd9steF6RSl2pn3aBV8U6gdHDtnFDMhmOANwZBZX1lDYsEWHH9hsFGaRzVFyXAQI6CqsXC391U48TyTnT5KvY9tbL2c5F44V6CCfekcrf2Z2YWv0qZwG/D+/Gvuf9Q2c8OWwyOeVuv/2ZaWmLij5D/ilwNORfiz8YrlQoKrR6uEkLz4RvJyOkghcdkYx8eyq/iV3Sa9xjurKb8+0JrgUw2yP/jg8CqCJcuocFs5C1komb9aHfm87wfH/GjD+1tvsQfIx0DtoZEJN7x5rCClX3uxhBUJusm2xXT+VK8HmCXYVXqQYcZBuBXCcJ86mZJZNxIyz8ppft89PbeX6cd0/W/VCCTb94FNrAGAlH4nFl0UXLMfhpLuzzbmpXAWK8mLas8ANge3QNd5TVlRYOVLforWBX+y8S8vDU5ogH+wB6IlSq39cfUWBVSnp1A/uf7B5I2Gm5Cg+bx0gMqk86BrxFHVMHRe2txbPEPNQ9L4Lv2ixrdMdDN1jIuRb82Fme+NCMi3rDMeaKWvEiEK9VUBjAjGbT3rdXMukaR8T3j+o1b9OSaUW/yOndJ4W9euD9GenRk5idJGMp5NpeLw6+1fMviGgn3HxkT1TqNBXHJQxjPmHmIYEfZBQkHkYOCbOQWOAxHoUm2RE8mmFxbf3ykkzz+YT4n1PRG0NKPSCTnlj2T/Eu0fPy0gSkWau/HErvVulNQ3K/y10DQ6L/jsMZv5MEcWqsr/LTT3QAVidHMXpKJ91o5kZOTMSoC5yON7dZFrXzQLvyyY5E2tf8WqWsiYa7uZueroANhRMAe6h+e7IQ1NW40Oi2Ioc73/rVd6lXKjzQ+UhCoHN+MceF20djLOlDG0CsrdYXvguk8029HyYrFra3E4aVMAOYaSx/FM52UVBW9WEnZCz7eRVPwHKjRZDzhsamztPQf+nuzH0uM1 Henk Batelaan
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    groups: [sudo, docker]
    shell: /bin/bash
packages:
  - apt-transport-https
  - ca-certificates
  - containerd.io
  - curl
  - gnupg-agent
  - dnsutils
  - docker-ce
  - docker-ce-cli
  - git
  - jq
  - nmap
  - psmisc
  - screen
  - tree
  - ufw
  - vim
package_update: true
package_upgrade: true
write_files:
  - path: /etc/ssh/sshd_config
    content: |
      PrintMotd no
      Banner none
      HostKey /etc/ssh/ssh_host_rsa_key
      HostKey /etc/ssh/ssh_host_ecdsa_key
      HostKey /etc/ssh/ssh_host_ed25519_key
      SyslogFacility AUTHPRIV
      LogLevel VERBOSE
      PermitRootLogin no
      MaxAuthTries 3
      MaxSessions 2
      AuthorizedKeysFile .ssh/authorized_keys
      PasswordAuthentication no
      ChallengeResponseAuthentication no
      GSSAPIAuthentication no
      GSSAPICleanupCredentials no
      UsePAM yes
      AllowAgentForwarding no
      AllowTcpForwarding no
      X11Forwarding no
      TCPKeepAlive no
      Compression no
      ClientAliveCountMax 2
      UseDNS no
      AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
      AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
      AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
      AcceptEnv XMODIFIERS
      Subsystem sftp /usr/lib/openssh/sftp-server
runcmd:
  - apt-get update
  - timedatectl set-timezone Europe/Amsterdam
  - timedatectl set-ntp on
  - ufw allow 22/tcp
  - ufw enable
  - sed -i -e '/pam_motd.so/s/^/# /g' /etc/pam.d/sshd
  - sed -i -e '/#startup_message/s/^#//' /etc/screenrc
  - wget https://github.com/docker/compose/releases/download/v2.2.2/docker-compose-linux-x86_64
  - mv docker-compose-linux-x86_64 docker-compose
  - chmod +x docker-compose 
  - mkdir -p /usr/local/lib/docker/cli-plugins
  - mv docker-compose /usr/local/lib/docker/cli-plugins
  - reboot
EOF
}
