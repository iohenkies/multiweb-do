# Lets enable backups by default
variable "backup" {
  type        = string
  default     = true
  description = "The Digital Ocean default is false, we want it to be true"
}

# Declaration of the Digital Ocean API token
variable "do_token" {
  type        = string
  description = "This is our token to authorize against the Digital Ocean API"
}

# Declaration of the region variable
variable "region" {
  type        = string
  default     = "ams3"
  description = "This is our region variable. The default is Amsterdam 3"
}


# Declaration of the size variable with a default size specified
variable "size" {
  type        = string
  default     = "s-1vcpu-1gb"
  description = "This is our size variable. The default is the smallest size possible"
}

# Default droplet tags
variable "tags" {
  type        = list(any)
  default     = ["ubuntu", "docker", "terraform"]
  description = "Just some default tags going with our droplet"
}
