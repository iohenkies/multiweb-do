# This will create the SSH key so we can login
resource "digitalocean_ssh_key" "henk_batelaan" {
  name       = "SSH Key Henk Batelaan"
  public_key = file("/Users/iohenkies/.ssh/id_rsa.pub")
}
